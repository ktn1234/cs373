#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# ----------------
# RangeIterator.py
# ----------------

def test0 () :
    x = range(2, 2)
    p = iter(x)
    assert hasattr(p, "__next__")
    assert hasattr(p, "__iter__")
    assert iter(p) is p
    try :
        next(p)
        assert False
    except StopIteration :
        pass

def test1 () :
    x = range(2, 3)
    p = iter(x)
    assert next(p) == 2
    try :
        next(p)
        assert False
    except StopIteration :
        pass

def test2 () :
    x = range(2, 4)
    p = iter(x)
    assert next(p) == 2
    assert next(p) == 3
    try :
        next(p)
        assert False
    except StopIteration :
        pass

def test3 () :
    x = range(2, 5)
    p = iter(x)
    a = [v for v in p]
    assert a == [2, 3, 4]

def test4 () :
    x = range(2, 5)
    p = iter(x)
    assert list(p) == [2, 3, 4]
    assert list(p) == []

def main () :
    print("RangeIterator.py")
    for n in range(5) :
        eval("test" + str(n) + "()")
    print("Done.")

if __name__ == "__main__" : # pragma: no cover
    main()
