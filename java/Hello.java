// ----------
// Hello.java
// ----------

final class Hello {
    public static void main (String[] args) {
        System.out.println("Nothing to be done.");}}

/*
Developed in 1995 by James Gosling of Canada, now at AWS.
Javas is procedural, object-oriented, statically typed, and garbage
collected.



% javac --version
javac 10.0.1



% java --version
java 10.0.1 2018-04-17



% javac -Xlint Hello.java
% java  -ea    Hello
Nothing to be done.
*/
