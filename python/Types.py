#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = duplicate-key
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = literal-comparison
# pylint: disable = missing-docstring
# pylint: disable = too-few-public-methods

# --------
# Types.py
# --------

# https://docs.python.org/3.6/library/types.html

from collections import deque
from types       import FunctionType

def test1 () :
    b = bool()
    assert b is False
    b = True
    assert b is True
    assert isinstance(b,    bool)
    assert isinstance(bool, type)

def test2 () :
    i = int()
    assert i == 0
    i = 2
    assert i == 2
    assert isinstance(i,   int)
    assert isinstance(int, type)

def test3 () :
    f = float()
    assert f == 0.0
    f = 2.34
    assert f == 2.34
    assert isinstance(f,     float)
    assert isinstance(float, type)

def test4 () :
    c = complex()
    assert c == 0 + 0j
    c = 2 + 3j
    assert c == 2 + 3j
    assert isinstance(c,       complex)
    assert isinstance(complex, type)

def test5 () :
    s = str()
    assert s is ""
    s = "abc"
    assert s is "abc"
    assert isinstance(s,   str)
    assert isinstance(str, type)

def test6 () :
    a = list()
    assert a ==     []
    assert a is not []
    a = [2, "abc", 3.45]
    assert a ==     [2, "abc", 3.45]
    assert a is not [2, "abc", 3.45]
    assert a !=     ["abc", 2, 3.45]
    assert isinstance(a,    list)
    assert isinstance(list, type)

def test7 () :
    u = tuple()
    assert u is ()
    u = (2, "abc", 3.45)
    assert u is (2, "abc", 3.45)
    assert u != ("abc", 2, 3.45)
    assert isinstance(u,     tuple)
    assert isinstance(tuple, type)

def test8 () :
    x = set()
    assert x ==     set()            # not {}
    assert x is not set()
    x = {2, 2, "abc", 3.45}
    assert x ==     {2, "abc", 3.45}
    assert x is not {2, "abc", 3.45}
    assert x ==     {"abc", 2, 3.45}
    assert isinstance(x,   set)
    assert isinstance(set, type)

def test9 () :
    y = frozenset()
    assert y is frozenset()
    y = frozenset((2, 2, "abc", 3.45))
    assert y ==     frozenset([2, "abc", 3.45])
    assert y ==     frozenset(["abc", 2, 3.45])
    assert y is not frozenset([2, "abc", 3.45])
    assert isinstance(y,         frozenset)
    assert isinstance(frozenset, type)

def test10 () :
    d = dict()
    assert d ==     {}
    assert d is not {}
    d = {2: "xxx", 2: "abc", 3: "def", 4: "ghi"}
    assert d ==     {2: "abc", 3: "def", 4: "ghi"}
    assert d ==     {3: "def", 2: "abc", 4: "ghi"}
    assert d is not {2: "abc", 3: "def", 4: "ghi"}
    assert isinstance(d,    dict)
    assert isinstance(dict, type)

def test11 () :
    q = deque()
    assert q ==     deque()
    assert q is not deque()
    q = deque((2, "abc", 3.45))
    assert q ==     deque((2, "abc", 3.45))
    assert q is not deque((2, "abc", 3.45))
    assert q !=     deque(("abc", 2, 3.45))
    assert isinstance(q,     deque)
    assert isinstance(deque, type)

def test12 () :
    def f (v) :
        return v + 1
    assert isinstance(f,            FunctionType)
    assert isinstance(FunctionType, type)

def test13 () :
    f = lambda v : v + 1
    assert isinstance(f,            FunctionType)
    assert isinstance(FunctionType, type)

def test14 () :
    class A :
        def __init__ (self, i, s, f) :
            self.i = i
            self.s = s
            self.f = f

    z = A(2, "abc", 3.45)
    assert z != A(2, "abc", 3.45)
    assert isinstance(z,    A)
    assert isinstance(z,    object)
    assert isinstance(A,    type)
    assert isinstance(type, type)

    assert issubclass(A, A)
    assert issubclass(A, object)

    assert issubclass(type, type)
    assert issubclass(type, object)

    assert issubclass(object, object)

def main () :
    print("Types.py")
    for i in range(14) :
        eval("test" + str(i + 1) + "()")
    print("Done.")

if __name__ == "__main__" : # pragma: no cover
    main()
