-- ---------
-- Join2.sql
-- ---------

use test;

-- -----------------------------------------------------------------------
select "*** drop table Apply ***";
drop table if exists Apply;

-- -----------------------------------------------------------------------
select "*** drop table College ***";
drop table if exists College;

-- -----------------------------------------------------------------------
select "*** drop table Student ***";
drop table if exists Student;

-- -----------------------------------------------------------------------
select "*** create table Student ***";
create table Student (
    sID         int not null,
    sName       text,
    GPA         float,
    sizeHS      int,
    primary key (sID))
    engine = innodb;

-- -----------------------------------------------------------------------
select "*** create table College ***";
create table College (
    cName       varchar(8) not null,
    state       char(2),
    enrollment  int,
    primary key (cName))
    engine = innodb;

-- -----------------------------------------------------------------------
select "*** create table Apply ***";
create table Apply (
    sID         int,
    cName       varchar(8),
    major       text,
    decision    boolean,
    foreign key (sID)   references Student (sID),
    foreign key (cName) references College (cName))
    engine = innodb;

-- -----------------------------------------------------------------------
-- ID, name, and GPA of students who applied in CS
-- MUST USE inner join

select "*** query #1 ***";

-- -----------------------------------------------------------------------
-- ID, name, and GPA of students who applied in CS
-- MUST USE subquery

select "*** query #2 ***";

-- -----------------------------------------------------------------------
-- GPA of students who applied in CS
-- sorted in descending order

select "*** query #3 ***";

-- -----------------------------------------------------------------------
-- ID of students who have applied in CS but not in EE

select "*** query #4 ***";

-- -----------------------------------------------------------------------
-- name and enrollment of college with highest enrollment

select "*** query #5 ***";

-- -----------------------------------------------------------------------
-- ID, name, and GPA of student with highest GPA

select "*** query #6 ***";

exit
