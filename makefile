.DEFAULT_GOAL := all

all:

clean:
	cd java; make clean
	@echo
	cd python; make clean

config:
	git config -l

init:
	git init
	git remote add origin git@gitlab.com:gpdowning/cs373.git
	git add README.md
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add java
	git add makefile
	git add notes
	git add python
	git add README.md
	git add sql
	git add uml
	git commit -m "another commit"
	git push
	git status

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

sync:
	cd java; make sync
	@echo
	cd python; make sync
	@echo
	cd sql; make sync
	@echo
	cd uml; make sync

versions:
	cd java; make versions
	@echo
	cd python; make versions
