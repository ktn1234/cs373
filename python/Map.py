#!/usr/bin/env python3

# pylint: disable = bad-whitespace
# pylint: disable = eval-used
# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = unidiomatic-typecheck

# ------
# Map.py
# ------

# https://docs.python.org/3/library/functions.html#map

def test0 () :
    m = map(lambda x : x ** 2, [2, 3, 4])
    assert     hasattr(m, "__next__")
    assert     hasattr(m, "__iter__")
    assert not hasattr(m, "__getitem__")
    assert not hasattr(m, "__len__")
    assert iter(m) is m
    assert list(m) == [4, 9, 16]
    assert list(m) == []

def test1 () :
    m = map(lambda x : x ** 3, (2, 3, 4))
    assert tuple(m) == (8, 27, 64)
    assert tuple(m) == ()

def test2 () :
    m = map(lambda x : x ** 2, {2, 3, 4})
    assert set(m) == {4, 9, 16}
    assert set(m) == set()

def test3 () :
    m = map(None, [])
    assert list(m) == []

def main () :
    print("Map.py")
    for n in range(4) :
        eval("test" + str(n) + "()")
    print("Done.")

if __name__ == "__main__" : # pragma: no cover
    main()
