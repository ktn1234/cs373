// ---------------
// Reflection.java
// ---------------

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

interface I
    {}

class A implements I {
    public A () {}}

class B implements I
    {}

class C implements I {
    public C (int i)
        {}}

class D implements I {
    private D () {}}

class E {
    public E () {}}

abstract class F {
    public F () {}}

interface G
    {}

final class Reflection {
    public static void test1 () {
        try {
            Class<?>       c = Class.forName("A");
            Constructor<?> r = c.getConstructor();
            I              x = (I) r.newInstance();}
        catch (ClassCastException e) {
            assert false;}
        catch (ClassNotFoundException e) {
            assert false;}
        catch (IllegalAccessException e) {
            assert false;}
        catch (InstantiationException e) {
            assert false;}
        catch (InvocationTargetException e) {
            assert false;}
        catch (NoSuchMethodException e) {
            assert false;}}

    public static void test2 () {
        try {
            Class<?>       c = Class.forName("B");
            Constructor<?> r = c.getConstructor();
            I              x = (I) r.newInstance();
            assert false;}
        catch (ClassCastException e) {
            assert false;}
        catch (ClassNotFoundException e) {
            assert false;}
        catch (IllegalAccessException e) {
            assert false;}
        catch (InstantiationException e) {
            assert false;}
        catch (InvocationTargetException e) {
            assert false;}
        catch (NoSuchMethodException e) {
            assert e.toString().substring(0, 31).equals("java.lang.NoSuchMethodException");}}

    public static void test3 () {
        try {
            Class<?>       c = Class.forName("C");
            Constructor<?> r = c.getConstructor();
            I              x = (I) r.newInstance();
            assert false;}
        catch (ClassCastException e) {
            assert false;}
        catch (ClassNotFoundException e) {
            assert false;}
        catch (IllegalAccessException e) {
            assert false;}
        catch (InstantiationException e) {
            assert false;}
        catch (InvocationTargetException e) {
            assert false;}
        catch (NoSuchMethodException e) {
            assert e.toString().substring(0, 31).equals("java.lang.NoSuchMethodException");}}

    public static void test4 () {
        try {
            Class<?>       c = Class.forName("D");
            Constructor<?> r = c.getConstructor();
            I              x = (I) r.newInstance();
            assert false;}
        catch (ClassCastException e) {
            assert false;}
        catch (ClassNotFoundException e) {
            assert false;}
        catch (IllegalAccessException e) {
            assert false;}
        catch (InstantiationException e) {
            assert false;}
        catch (InvocationTargetException e) {
            assert false;}
        catch (NoSuchMethodException e) {
            assert e.toString().substring(0, 31).equals("java.lang.NoSuchMethodException");}}

    public static void test5 () {
        try {
            Class<?>       c = Class.forName("E");
            Constructor<?> r = c.getConstructor();
            I              x = (I) r.newInstance();
            assert false;}
        catch (ClassCastException e) {
            assert e.toString().substring(0, 28).equals("java.lang.ClassCastException");}
        catch (ClassNotFoundException e) {
            assert false;}
        catch (IllegalAccessException e) {
            assert false;}
        catch (InstantiationException e) {
            assert false;}
        catch (InvocationTargetException e) {
            assert false;}
        catch (NoSuchMethodException e) {
            assert false;}}

    public static void test6 () {
        try {
            Class<?>       c = Class.forName("F");
            Constructor<?> r = c.getConstructor();
            I              x = (I) r.newInstance();
            assert false;}
        catch (ClassCastException e) {
            assert false;}
        catch (ClassNotFoundException e) {
            assert false;}
        catch (IllegalAccessException e) {
            assert false;}
        catch (InstantiationException e) {
            assert e.toString().equals("java.lang.InstantiationException");}
        catch (InvocationTargetException e) {
            assert false;}
        catch (NoSuchMethodException e) {
            assert false;}}

    public static void test7 () {
        try {
            Class<?>       c = Class.forName("G");
            Constructor<?> r = c.getConstructor();
            I              x = (I) r.newInstance();
            assert false;}
        catch (ClassCastException e) {
            assert false;}
        catch (ClassNotFoundException e) {
            assert false;}
        catch (IllegalAccessException e) {
            assert false;}
        catch (InstantiationException e) {
            assert false;}
        catch (InvocationTargetException e) {
            assert false;}
        catch (NoSuchMethodException e) {
            assert e.toString().substring(0, 31).equals("java.lang.NoSuchMethodException");}}

    public static void test8 () {
        try {
            Class<?>       c = Class.forName("H");
            Constructor<?> r = c.getConstructor();
            I              x = (I) r.newInstance();
            assert false;}
        catch (ClassCastException e) {
            assert false;}
        catch (ClassNotFoundException e) {
            assert e.toString().substring(0, 32).equals("java.lang.ClassNotFoundException");}
        catch (IllegalAccessException e) {
            assert false;}
        catch (InstantiationException e) {
            assert false;}
        catch (InvocationTargetException e) {
            assert false;}
        catch (NoSuchMethodException e) {
            assert false;}}

    public static void main (String[] args) {
        System.out.println("Reflection.java");
        test1();
        test2();
        test3();
        test4();
        test5();
        test6();
        test7();
        test8();
        System.out.println("Done.");}}
